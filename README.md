# Scroll Spy

No need for a definition. Scroll spy, do what its supposed to do, spies on the scrolling event, and trigger custom events, when defined conditions (based uppon scroll) are fulfilled.

### Example

For each image that the image switcher must watch:

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="functions.js"></script>
<script type="text/javascript" src="utils.js"></script>
<script type="text/javascript" src="scroll-spy.js"></script>
```

Then just initiate the spy and add some listeners:

```javascript
// Initializes the spy
var spy = new CRD.ScrollSpy();

// Add some custom listeners
spy.at(250, function() {
	console.log('250px scrolled!');
});

// It also accepts negative values
spy.at('-10%', function() {
	console.log('10% to reach bottom of the page!');
});
```

### Arguments

```CRD.ScrollSpy``` accepts a single argumens:

Argument | Default | Description
-------- | ------- | -----------
element : ```Object``` | ```window``` | Element to spy for scrolling events.

### Events

Event | Params | Description
----- | ------ | -----------
```crd.scrollspy.scroll``` | scrollY : ```Number```, object : ```CRD.ScrollSpy``` | Triggers every time elements get scrolled.
```crd.scrollspy.up``` | scrollY : ```Number, object : ```CRD.ScrollSpy``` | Triggers when element is scrolling up.
```crd.scrollspy.down``` | scrollY : ```Number```, object : ```CRD.ScrollSpy``` | Triggers when element is scrolling down.

### Methods

Method | Arguents | Returns | Description
------ | -------- | ------- | -----------
constructor | element : ```Object``` | A reference to the current object or class: ```CRD.ScrollSpy```. | Initializes the instance with the provided parameters.
evalScrollPosition | --- | --- | Evaluates the scroll position to trigger specific actions defined for the object.
at | position : ```String``` or ```Number```, callback : ```Function```, once : ```Boolean``` | A reference to the current object or class: ```CRD.ScrollSpy```. | Adds a custom event o be executed at a defined scrolling position or defined by a custom modifier.
parsePosition | position : ```String``` or ```Number```, size : ```Number``` | ```Number``` or ```Boolean``` representing the parsed property (number, %, or unit defined by a modifier). | Parse a specific position according to the scrollable element size.
addModifier | keyword : ```String```, callback : ```Function``` | A reference to the current object or class: ```CRD.ScrollSpy```. | Adds a custom modifier to parse user defined units.
destroy | --- | --- | Unhooks from listened events.

### Constants

Property | Type | Description
------ | -------- | -----------
CRD.ScrollSpy.Scroll.UP | ```String``` | Indicates the "scrolling up" event.
CRD.ScrollSpy.Scroll.DOWN | ```String``` | Indicates the "scrolling down" event.

### Customization

```CRD.ScrollSpy``` accepts the definition of custom modifiers (using ```addModifier``` method) to parse the measurement units.

Custom modifiers can be defined in order to specify specific units or rules, acording, for example, to dom elements or predefined values.

The modifier callback must accepts the following parameters:

Param | Description
------ | -------
total | Window scroll (Y) amount.
value | Multiplier value.

The callback function must return a numeric value (without units).

#### Example

```javascript
// Adds custom modifier, to bind to a specific dom element
CRD.ScrollSpy.addModifier('element', function(total, value) {
	return jQuery('#element').height() * value;
});

// Initializes the spy
var spy = new CRD.ScrollSpy();

// Add some custom listeners
spy.at('1 element', function() {
	console.log('Scrolled one element height!');
});
```

### Dependencies

- CRD.Utils
- JQuery [http://www.jquery.com](jQuery)

### License

Copyright (c) 2016 Luciano Giordano

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
